# Touch Surgery - Test Web Assignment

---

## Instructions

This assignment will test your skills with test automation and your selected language

1. Using a test automation framework, create a script (as `complex` or `simple` as you wish) to:
    1. Open the website [https://www.reddit.com/](https://www.reddit.com/)
    1. Search for a subreddit called `"gaming"`
    1. Open the sub-reddit
    1. Print out the top most post's title
    1. Perform a login
    1. Downvote the second post if it's upvoted already, upvote otherwise (in case the second post is an advertisement or announcement, use the third)
1. After you finish, *zip all your source code* and email us back

Extra points for:

1. Usage of BDD
1. Page-object pattern
1. Usage of good code standards
1. Documentation
1. Javascript/WebdriverIO (but remember, it's not necessary)